const express = require('express');
const https = require('https');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');

dotenv.config();
const app = express();

const pages = {
    error: __dirname + '/pages/error.html',
    success: __dirname + '/pages/success.html',
    home: __dirname + '/pages/index.html',
};

app.use(express.static(process.env.public_dir));
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (req, res, next) => {
    console.log('i reached here')
    res.sendFile(pages.home);
});

app.post('/users/newsletter/subscribe', (req, res) => {
    //get user details
    const user = {fname, lname, email} = req.body

    //ensure user provided all data
    if(!(Object.keys(user).length === 3)) {
        res.sendFile(pages.error);

        //validate fields
        //...
    }

    const userData = JSON.stringify({
        members: [
            {
                email_address: user.email,
                status: 'subscribed',
                merge_fields: {
                    FNAME: user.fname,
                    LNAME: user.lname
                }
            }
        ]
    });

    //subscribe to mailchimp
    const options = {
        method: 'POST',
        auth: 'noslac:' + process.env.MC_KEY
    }

    const request = https.request(process.env.MC_REQUEST_URL, options, response => {
        //an error occurred somewhere
        if(response.statusCode > 299 && response.statusCode <=  599) {
            res.sendFile(pages.success);
        } else {
            res.sendFile(pages.error)
        }
    });

    request.write(userData);

    request.on('error', (e) => {
        console.error(e);
    });

    request.end();
    // res.sendFile(pages.success);
});

//process.env.PORT = heroku's port and process.env.port = localport
app.listen(process.env.PORT || process.env.port, () => {
    console.log(`Listening on port ${process.env.port}`);
})